﻿using System;

using SlimDX.DirectInput;


namespace SlimFramework
{
	// This class manages user input functionality.
	public class UserInput : IDisposable
    {

        // MEMBER VARIABLES
        // ======================================================================================================================
        
        bool m_IsDisposed = false;

        // Our DirectInput object is held in this variable.
        DirectInput m_DirectInput;

        // Our keyboard variables.
        Keyboard m_Keyboard;
        KeyboardState m_KeyboardStateCurrent;
        KeyboardState m_KeyboardStateLast;

        // Our mouse variables.
        Mouse m_Mouse;
        MouseState m_MouseStateCurrent;
        MouseState m_MouseStateLast;




        // CONSTRUCTORS
        // ======================================================================================================================

        /// <summary>
        /// The constructor.
        /// </summary>
        public UserInput()
        {
            InitDirectInput();

            // We need to intiailize these because otherwise we will get a null reference error
            // if the program tries to access these on the first frame.
            m_KeyboardStateCurrent = new KeyboardState();
            m_KeyboardStateLast = new KeyboardState();

            m_MouseStateCurrent = new MouseState();
            m_MouseStateLast = new MouseState();
        }




        // NON-PUBLIC METHODS
        // ======================================================================================================================

        /// <summary>
        /// This method initializes DirectInput.
        /// </summary>
        private void InitDirectInput()
        {
            m_DirectInput = new DirectInput();
            if (m_DirectInput == null)
                return; // An error has occurred, initialization of DirectInput failed for some reason so simply return from this method.


            // Create our keyboard and mouse devices.
            m_Keyboard = new Keyboard(m_DirectInput);
            if (m_Keyboard == null)
                return;  // An error has occurred, initialization of the keyboard failed for some reason so simply return from this method.
            
            m_Mouse = new Mouse(m_DirectInput);
            if (m_Mouse == null)
                return; // An error has occurred, initialization of the mouse failed for some reason so simply return from this method.
            
        }


        /// <summary>
        /// This function updates the state variables.  It should be called from the game's UpdateScene() function before
        /// it does any input processing.  
        /// </summary>
        public void Update()
        {

            // Reacquire the devices in case another application has taken control of them and check for errors.
            if (m_Keyboard.Acquire().IsFailure ||
                m_Mouse.Acquire().IsFailure)
            {
                // We failed to successfully acquire one of the devices so abort updating the user input stuff by simply returning from this method.
                return;
            }


            // Update our keyboard state variables.
            m_KeyboardStateLast = m_KeyboardStateCurrent;
            m_KeyboardStateCurrent = m_Keyboard.GetCurrentState();




            // NOTE: All of the if statements below are for testing purposes.  In a real program, you would remove them or comment them out
            //       and then recompile before releasing your game.  This is because we don't want debug code slowing down the finished game


            // This is our test code for keyboard input via DirectInput.
            if (IsKeyPressed(Key.Space))
                System.Diagnostics.Debug.WriteLine("DIRECTINPUT: KEY SPACE IS PRESSED!");
            if (IsKeyHeldDown(Key.Space))
                System.Diagnostics.Debug.WriteLine("DIRECTINPUT: KEY SPACE IS HELD DOWN!");
            if (IsKeyPressed(Key.Z))
                System.Diagnostics.Debug.WriteLine("DIRECTINPUT: KEY Z IS PRESSED!");



            // Update our mouse state variables.
            m_MouseStateLast = m_MouseStateCurrent;
            m_MouseStateCurrent = m_Mouse.GetCurrentState();

            // This is our test code for mouse input via DirectInput.
            if (IsMouseButtonPressed(0))
                System.Diagnostics.Debug.WriteLine("DIRECTINPUT: LEFT MOUSE BUTTON IS PRESSED!");
            if (IsMouseButtonPressed(1))
                System.Diagnostics.Debug.WriteLine("DIRECTINPUT: RIGHT MOUSE BUTTON IS PRESSED!");
            if (IsMouseButtonPressed(2))
                System.Diagnostics.Debug.WriteLine("DIRECTINPUT: MIDDLE MOUSE BUTTON IS PRESSED!");

        }




        // KEYBOARD/MOUSE METHODS
        // ======================================================================================================================

        /// <summary>
        /// This method checks if the specified key is pressed.
        /// </summary>
        /// <param name="key">The key to check the state of.</param>
        /// <returns>True if the key is pressed or false otherwise.</returns>
        public bool IsKeyPressed(Key key)
        {
            return m_KeyboardStateCurrent.IsPressed(key);
        }


        /// <summary>
        /// This method checks if the specified key was pressed during the previous frame.
        /// </summary>
        /// <param name="key">The key to check the state of.</param>
        /// <returns>True if the key was pressed during the previous frame, or false otherwise.</returns>
        public bool WasKeyPressed(Key key)
        {
            return m_KeyboardStateLast.IsPressed(key);
        }

        /// <summary>
        /// This method checks if the specified key is released.
        /// </summary>
        /// <param name="key">The key to check the state of.</param>
        /// <returns>True if the key is released or false otherwise.</returns>
        public bool IsKeyReleased(Key key)
        {
            return m_KeyboardStateCurrent.IsReleased(key);
        }

        /// <summary>
        /// This method checks if the specified key was released (not pressed) during the previous frame.
        /// </summary>
        /// <param name="key">The key to check the state of.</param>
        /// <returns>True if the key was not pressed during the previous frame, or false otherwise.</returns>
        public bool WasKeyReleased(Key key)
        {
            return m_KeyboardStateLast.IsReleased(key);
        }

        /// <summary>
        /// This method checks if the specified key is held down (meaning it has been held down for 2 or more consecutive frames).
        /// </summary>
        /// <param name="key">The key to check the state of.</param>
        /// <returns>True if the key is being held down or false otherwise.</returns>
        public bool IsKeyHeldDown(Key key)
        {
            return (m_KeyboardStateCurrent.IsPressed(key) && m_KeyboardStateLast.IsPressed(key));
        }




        /// <summary>
        /// This method checks if the specified mouse button is pressed.
        /// </summary>
        /// <param name="button">The button to check the state of. 0 = left button, 1 = right button, 2 = middle button</param>
        /// <returns>True if the button is pressed or false otherwise.</returns>
        public bool IsMouseButtonPressed(int button)
        {
            return m_MouseStateCurrent.IsPressed(button);
        }

        /// <summary>
        /// This method checks if the specified mouse button was pressed during the previous frame.
        /// </summary>
        /// <param name="button">The button to check the state of. 0 = left button, 1 = right button, 2 = middle button</param>
        /// <returns>True if the button was pressed during the previous frame or false otherwise.</returns>
        public bool WasMouseButtonPressed(int button)
        {
            return m_MouseStateLast.IsPressed(button);
        }

        /// <summary>
        /// This method checks if the specified mouse button is pressed.
        /// </summary>
        /// <param name="button">The button to check the state of. 0 = left button, 1 = right button, 2 = middle button.</param>
        /// <returns>True if the button is released or false otherwise.</returns>
        public bool IsMouseButtonReleased(int button)
        {
            return m_MouseStateCurrent.IsReleased(button);
        }

        /// <summary>
        /// This method checks if the specified mouse button was released (not pressed) during the previous frame.
        /// </summary>
        /// <param name="button">The button to check the state of. 0 = left button, 1 = right button, 2 = middle button</param>
        /// <returns>True if the button was released (not pressed) during the previous frame or false otherwise.</returns>
        public bool WasMouseButtonReleased(int button)
        {
            return m_MouseStateLast.IsReleased(button);
        }

        /// <summary>
        /// This method checks if the specified mouse button is being held down (meaning it has been held down for 2 or more consecutive frames).
        /// </summary>
        /// <param name="button">The button to check the state of. 0 = left button, 1 = right button, 2 = middle button</param>
        /// <returns>True if the button is held down or false otherwise.</returns>
        public bool IsMouseButtonHeldDown(int button)
        {
            return (m_MouseStateCurrent.IsPressed(button) && m_MouseStateLast.IsPressed(button));
        }

        /// <summary>
        /// This method checks if the mouse has moved since the previous frame.
        /// </summary>
        /// <returns>True if the mouse has moved since the previous frame, or false otherwise.</returns>
        public bool MouseHasMoved()
        {
            if ((m_MouseStateCurrent.X != m_MouseStateLast.X) ||
                (m_MouseStateCurrent.Y != m_MouseStateLast.Y))
            {
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// This method gets the mouse position for the current frame.
        /// </summary>
        /// <returns>A System.Drawing.Point object containing the current mouse position.</returns>
        public System.Drawing.Point MousePosition()
        {
            return new System.Drawing.Point(m_MouseStateCurrent.X, m_MouseStateCurrent.Y);
        }

        /// <summary>
        /// This method gets the mouse position for the previous frame.
        /// </summary>
        /// <returns>A System.Drawing.Point object containing the mouse's position during the previous frame.</returns>
        public System.Drawing.Point LastMousePosition()
        {
            return new System.Drawing.Point(m_MouseStateLast.X, m_MouseStateLast.Y);
        }

        /// <summary>
        /// This method gets the scrollwheel value in most cases.
        /// Note that this value is a delta, or in other words it is the amount the scroll wheel has been moved
        /// since the last frame.
        /// </summary>
        /// <returns>The amount the scroll wheel has moved.  This can be positive or negative depending on which way it has moved.</returns>
        public int MouseWheelMovement()
        {
            return m_MouseStateCurrent.Z;
        }





        // INTERFACE METHODS
        // ======================================================================================================================

        // This section is for methods that are part of the interfaces that the class implements.


        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);

            // Since this Dispose() method already cleaned up the resources used by this object, there's no need for the
            // Garbage Collector to call this class's Finalizer, so we tell it not to.
            // We did not implement a Finalizer for this class as in our case we don't need to implement it.
            // The Finalize() method is used to give the object a chance to clean up its unmanaged resources before it
            // is destroyed by the Garbage Collector.  Since we are only using managed code, we do not need to
            // implement the Finalize() method.
            GC.SuppressFinalize(this);

        }

        protected void Dispose(bool disposing)
        {
            if (!this.m_IsDisposed)
            {
                /*
                * The following text is from MSDN  (http://msdn.microsoft.com/en-us/library/fs2xkftw%28VS.80%29.aspx)
                * 
                * 
                * Dispose(bool disposing) executes in two distinct scenarios:
                * 
                * If disposing equals true, the method has been called directly or indirectly by a user's code and managed and unmanaged resources can be disposed.
                * If disposing equals false, the method has been called by the runtime from inside the finalizer and only unmanaged resources can be disposed. 
                * 
                * When an object is executing its finalization code, it should not reference other objects, because finalizers do not execute in any particular order. 
                * If an executing finalizer references another object that has already been finalized, the executing finalizer will fail.
                */
                if (disposing)
                {
                    // Unregister events


                    // get rid of managed resources
                    if (m_DirectInput != null)
                        m_DirectInput.Dispose();

                    if (m_Keyboard != null)
                        m_Keyboard.Dispose();

                    if (m_Mouse != null)
                        m_Mouse.Dispose();


                }

                // get rid of unmanaged resources

            }

        }




        // PROPERTIES
        // ======================================================================================================================

        /// <summary>
        /// Returns a boolean value indicating whether or not this object has been disposed.
        /// </summary>
        public bool IsDisposed
        {
            get
            {
                return m_IsDisposed;
            }
        }

        /// <summary>
        /// Gets the keyboard object.
        /// </summary>
        public Keyboard Keyboard
        {
            get
            {
                return m_Keyboard;
            }
        }

        /// <summary>
        /// Gets the keyboard state for the current frame.
        /// </summary>
        public KeyboardState KeyboardState_Current
        {
            get
            {
                return m_KeyboardStateCurrent;
            }
        }

        /// <summary>
        /// Gets the keyboard state from the previous frame.
        /// </summary>
        public KeyboardState KeyboardState_Previous
        {
            get
            {
                return m_KeyboardStateLast;
            }
        }

        /// <summary>
        ///  Gets the mouse object.
        /// </summary>
        public Mouse Mouse
        {
            get
            {
                return m_Mouse;
            }
        }

        /// <summary>
        /// Gets the mouse state for the current frame.
        /// </summary>
        public MouseState MouseState_Current
        {
            get
            {
                return m_MouseStateCurrent;
            }
        }

        /// <summary>
        /// Gets the mouse state from the previous frame.
        /// </summary>
        public MouseState MouseState_Previous
        {
            get
            {
                return m_MouseStateLast;
            }
        }


    }
}
